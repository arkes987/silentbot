#ifndef CONTROL_H
#define CONTROL_H
#include <string>
#include <Windows.h>
#include "memory.h"
#include <iostream>

class Control
{
public:
	Control();
	Control(LPCSTR _ProcName);

private:
	HWND m_hwnd;

public:
	void Press(std::string msg);
	void Press(char &msg);
	void Send_VK(WORD vk);

};
#endif
