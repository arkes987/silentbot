#include "stdafx.h"
#include <iostream>
#include "consts.h"
#include "memory.h"
#include "control.h"
#include <stdint.h>
#include <ctime>

class Player
{
public:
	Player(Memory &_Mem) : m_Mem(_Mem) {}

	void Update_Player()
	{ 
		m_ID = m_Mem.Read<uint16_t>(m_Mem.m_ProcessBaseAddress + PLAYER_ID_OFFSET);
		m_HP = m_Mem.Read<int32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_HP_OFFSET);
		m_MP = m_Mem.Read<int32_t>(m_Mem.m_ProcessBaseAddress + PLAYER_MP_OFFSET);
	}

private:
	Memory &m_Mem;

public:
	uint16_t m_ID;
	int32_t m_HP;
	int32_t m_MP;
};

class Creatures
{
public:
	Creatures(Memory &_Mem, Player &_LocalPlayer) : m_Mem(_Mem), m_LocalPlayer(_LocalPlayer){}
private:
	Memory &m_Mem;
	Player &m_LocalPlayer;
public :
	BattleList m_Battle;

	void Update_Battlelist()
	{
		m_Mem.Read(m_Mem.m_ProcessBaseAddress + BATTLELIST_OFFSET, &m_Battle.List[0], sizeof(Creature) * 250);
		m_Battle.IdList.clear();
		for (auto &c : m_Battle.List)
			if (c.DistanceID != 0)
				m_Battle.IdList[c.DistanceID] = &c;
	}

	inline Creature* GetLocalCreature()
	{
		return m_Battle.IdList[m_LocalPlayer.m_ID];
	}

};

class Containers
{
public:
	Containers(Memory &_Mem) : m_Mem(_Mem) {}
	ContainerEntry Entry[16];

	void Update_Containers()
	{
		m_Mem.Read<ContainerEntry[16]>(m_Mem.m_ProcessBaseAddress + CONTAINER_START_OFFSET, &Entry);
	}

	ContainerEntry& operator [](unsigned int id)
	{
		return Entry[id];
	}

private:
	Memory &m_Mem;
};


int main()
{
	Control k("Tibia");
	Memory mem("Kovloria.exe");
	Player p(mem);
	Creatures c(mem, p);
	Containers cont(mem);


	while (mem.IsOpened())
	{
		p.Update_Player();
		cont.Update_Containers();
		for (int i = 0; i < 16; ++i)
			if (cont[i].DistanceID != 0)
			{
				std::cout << cont[i].DistanceName << "\n";
				for (int j = 0; j < cont[i].DistanceAmount; ++j)
				{
					std::cout << "\t" << cont[i][j].DistanceItemId << ": "
						<< cont[i][j].DistanceItemCount << "\n";
				}
			}
		Sleep(1000);
		system("cls");
	
	}
	return 0;
}

