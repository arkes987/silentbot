#include "stdafx.h"
#include "control.h"


Control::Control() {}

Control::Control(LPCSTR _ProcName) : Control()
{
	m_hwnd = FindWindow(NULL, _ProcName);

}

//f
void Control::Press(std::string msg)
{
	if (!msg.empty())
	{
		for (auto &m : msg)
		{
			PostMessage(m_hwnd, WM_CHAR, m, 0);
		}
		Send_VK(0x0D);
	}
}

void Control::Press(char &msg)
{
	PostMessage(m_hwnd, WM_CHAR, msg, 0);
}

void Control::Send_VK(WORD vk)
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;
	ip.ki.wVk = vk;
	ip.ki.dwFlags = 0; 
	SendInput(1, &ip, sizeof(INPUT));
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}