#ifndef MEMORY_H
#define MEMORY_H

#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include <stdint.h>
#include <vector>
#include <map>

struct Vector
{
public:
	int X, Y, Z;
};


class Creature
{
public:
	uint16_t DistanceID; //0x0000 
	uint16_t DistanceType; //0x0002 
	char DistanceName[24]; //0x0004 
	int8_t _0x001C[8];
	int32_t DistanceX; //0x0024 
	int32_t DistanceY; //0x0028 
	int32_t DistanceZ; //0x002C 
	int32_t DistanceScreenOffsetHoriz; //0x0030 
	int32_t DistanceScreenOffsetVer; //0x0034 
	int8_t _0x0038[20];
	int32_t DistanceIsWalking; //0x004C 
	int32_t DistanceDirection; //0x0050 
	int8_t _0x0054[8];
	int32_t DistanceOutfit; //0x005C 
	int32_t DistanceColorHead; //0x0060 
	int32_t DistanceColorBody; //0x0064 
	int32_t DistanceColorLegs; //0x0068 
	int32_t DistanceColorFeet; //0x006C 
	int32_t DistanceAddon; //0x0070 
	int32_t DistanceLight; //0x0074 
	int32_t DistanceLightColor; //0x0078 
	int8_t _0x007C[8];
	int32_t DistanceBlackSquare; //0x0084 
	int32_t DistanceHPBar; //0x0088 
	int32_t DistanceWalkSpeed; //0x008C 
	int32_t DistanceIsVisible; //0x0090 
	int32_t DistanceSkull; //0x0094 
	int32_t DistanceParty; //0x0098 
	int8_t _0x009C[4];
	int32_t DistanceWarIcon; //0x00A0 
	int32_t DistanceIsBlocking; //0x00A4 

};

class Item
{
public:
	__int32 DistanceItemId; //0x0000 
	__int32 DistanceItemCount; //0x0004 
	__int32 N00BE250D; //0x0008 

};//Size=0x000C

class ContainerEntry
{
public:
	__int32 DistanceIsOpen; //0x0000 
	__int32 DistanceID; //0x0004 
	char _0x0008[8];
	char DistanceName[24]; //0x0010 
	char _0x0028[8];
	__int32 DistanceVolume; //0x0030 
	char _0x0034[4];
	__int32 DistanceAmount; //0x0038 
	Item DistanceItems[36]; //0x003C 

	Item& operator [](unsigned int id)
	{
		return DistanceItems[id];
	}
};

class BattleList
{
public:
	BattleList() : List(250)
	{

	}
	std::vector<Creature> List;
	std::map<int16_t, Creature*> IdList; //mapa id -> &List[]
};


class Memory
{
	friend class Control;
public:
	Memory();
	Memory(std::string _ProcName);
	Memory(unsigned int _PID);
	~Memory();

	inline bool IsOpened();
	HANDLE GetModuleHandle(std::string _ModuleName);

	int Read(unsigned int _Address, void *ptr, size_t size);

	template<typename T>
	int Read(unsigned int _Address, T *t)
	{
		return ReadProcessMemory(m_Handle, (LPCVOID)_Address, t, sizeof(T), 0);
	}

	template<typename T>
	T Read(unsigned int _Address)
	{
		T temp;
		ReadProcessMemory(m_Handle, (LPCVOID)_Address, &temp, sizeof(T), 0);
		return temp;
	}

private:
	void Open();

public:
	unsigned int m_ProcessBaseAddress;

private:
	HANDLE m_Handle;
	unsigned int m_PID;
};
#endif 

